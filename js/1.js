$(() => {
    $('.hd_arr').click(() => {
        $("html, body").animate({
            scrollTop: $('.st3').offset().top - 63.19 - 44
        });
    });

    new WOW().init();
    $(window).scroll((event) => {
        var vitri = $('html, body').scrollTop();
        if (vitri >= 32) {
            $('.hd_menu').addClass('menufixtop');
            $('.imgtop').addClass('menufixtopimg');
        } else {
            $('.hd_menu').removeClass('menufixtop');
            $('.imgtop').removeClass('menufixtopimg');
        }
    });
    var owl = $('.owl-st3_sld');
    owl.owlCarousel({
        margin: 35,
        navText: ['<div class ="koko-prev"><i class="koko-next" aria-hidden="true"></i></div>', '<i class="koko-next" aria-hidden="true"></i>'],
        nav: true,
        dots: false,
        loop: false,
        autoplay: false,
        autoplaySpeed: 1000,
        autoplayTimeout: 4000,
        responsive: {
            0: {
                items: 1
            },
            768: {
                items: 2
            },
            1266: {
                items: 3
            }
        }
    });
    $(window).scroll(() => {
        if ($(this).scrollTop() > 1000) {
            $('.scrollup').fadeIn();
            $('.scrollup').addClass('move_top');
        } else {
            $('.scrollup').fadeOut();
            $('.scrollup').removeClass('move_top');
        }
    });
    $('.scrollup').click(() => {
        $("html, body").animate({
            scrollTop: 0
        }, 600);
        return false;
    });
    $('.sp-cs_adv').click(() => {
        $('.cs_adv').addClass('show_');
        $('.cs_bas').removeClass('show_');
        $('.sp-cs_adv').addClass('active_');
        $('.sp-cs_bas').removeClass('active_');
    });
    $('.sp-cs_bas').click(() => {
        $('.cs_adv').removeClass('show_');
        $('.cs_bas').addClass('show_');
        $('.sp-cs_adv').removeClass('active_');
        $('.sp-cs_bas').addClass('active_');
    });
    $('.dc1_box').slideUp();
    $('.dc1_li').click(() => {
        $(this).next('.dc1_box').slideToggle();
        $(this).children().toggleClass('showicon');
        $(this).toggleClass('changecolor');
    });
    $('.owl-st7sld').owlCarousel({
        margin: 35,
        navText: ['<div class ="koko-prev"><i class="koko-next" aria-hidden="true"></i></div>', '<i class="koko-next" aria-hidden="true"></i>'],
        nav: true,
        dots: false,
        loop: false,
        autoplay: false,
        autoplaySpeed: 1000,
        autoplayTimeout: 4000,
        responsive: {
            0: {
                items: 1
            },
            768: {
                items: 2
            },
            1266: {
                items: 4
            }
        }
    });
    $(".lkg_ip").on('click', () => {
        $('.lkg_ip').removeClass('border_');
        $(this).addClass('border_');
    });
    $('#no-more-tables button').click(() => {
        $('.lkg_form').addClass('lkg_show');
        $('.lkg_bgFull').addClass('lkg_show');

    })
    $('.lkg_bgFull').click(() => {
        $('.lkg_form').removeClass('lkg_show');
        $('.lkg_bgFull').removeClass('lkg_show');
    })
    $('.lkg_remove').click(() => {
        $('.lkg_form').removeClass('lkg_show');
        $('.lkg_bgFull').removeClass('lkg_show');
    })
});